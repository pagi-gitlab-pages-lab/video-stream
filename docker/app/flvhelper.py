import argparse
import time, json, os, re
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

import configparser

# enable browser logging
d = DesiredCapabilities.CHROME
d['loggingPrefs'] = { 'browser':'ALL' }

driver = None


def initDriver(url):
    print("Loading...")
    global driver
    chrome_options = Options()
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-gpu')
    chrome_options.add_argument('--disable-dev-shm-usage')
    driver = webdriver.Chrome(chrome_options=chrome_options,desired_capabilities=d)
    driver.get(url)
    driver.set_window_size(1024, 768)
    print("Chrome Drive startup done...")

def quit():
    print("Quitting...")
    driver.quit()
    

### entry ###
def main(args):
    logPeroid = 15
    print(args.video)
    print(args.output)
    try:
        initDriver(args.video)
        f = open(args.output, 'w+')
        while True:
            print('loop...')
            totalfps = 0.0
            count = 0
            for log in driver.get_log('browser'):
                print(log)
                if 'message' in log:
                    if log['message'].find('fps:') > 0:  
                        pattern = '.*"fps: (.*)"$'
                        fps_str = re.search(pattern, log['message']).groups()[0]
                        if fps_str != 'undefined':
                            fps = float(fps_str)
                            totalfps += fps
                            count = count + 1
                        else:
                            fps = 0
                            count = count + 1
            if count > 0:
                fps = totalfps / count            
                text = "FPS=%d" % (fps)
                print(text)
                f.seek(0)
                f.write(text)
                f.truncate()
            time.sleep(logPeroid)
        
    except Exception as e:
        print(e)

if __name__ == "__main__":
    print("Starting up...")
    parser = argparse.ArgumentParser(description='pyVideoHelper: Python Video Helper')
    parser.add_argument('-v', '--video', help='video url')
    parser.add_argument('-o', '--output', help='log output')
    main(parser.parse_args())
