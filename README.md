# video-stream

可以透過 gitlab pages 來放置一些播放器 提供監控使用   
網頁放置於 pages目錄下

## 開發測試頁面
可以透過 localserver的docker-compose.yml, 啟動一台本地的nginx    
之後連 127.0.0.1/__page_name__ 就可以測試


### 輸出fps至檔案(flv)
```
python flvhelper.py -v 'https://pagi-gitlab-pages-lab.gitlab.io/video-stream/pages/test.html?video=https://1011.hlsplay.aodianyun.com/demo/game.flv' -o 'pagi.log'

via docker image

docker run -v '/tmp/log:/log' registry.gitlab.com/pagi-gitlab-pages-lab/video-stream:v0.0.1 python /app/flvhelper.py -v 'https://pagi-gitlab-pages-lab.gitlab.io/video-stream/pages/test.html?video=https://1011.hlsplay.aodianyun.com/demo/game.flv' -o '/log/pagi2.log'

```
