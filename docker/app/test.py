import argparse
import time
import requests
import os
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

import configparser

# enable browser logging
d = DesiredCapabilities.CHROME
d['loggingPrefs'] = { 'browser':'ALL' }

driver = None


def launch():
    print("Loading...")
    global driver
    chrome_options = Options()
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-gpu')
    chrome_options.add_argument('--disable-dev-shm-usage')
    driver = webdriver.Chrome(chrome_options=chrome_options,desired_capabilities=d)
    # driver = webdriver.Chrome()
    driver.get('https://pagi-gitlab-pages-lab.gitlab.io/video-stream/pages/test.html?video=https://1011.hlsplay.aodianyun.com/demo/game.flv')
    driver.set_window_size(1024, 768)
    print("Loaded Skype!")

def signIn(userName, password):
    print("Signing in...")
    userNameField = driver.find_element_by_name("loginfmt")
    userNameField.clear()
    userNameField.send_keys(userName)

    time.sleep(2)
    idSIButton9 = driver.find_element_by_id("idSIButton9")
    idSIButton9.click()

    time.sleep(10)
    print (driver.current_url)
    passwordField = driver.find_element_by_name("passwd")
    passwordField.clear()
    passwordField.send_keys(password)

    time.sleep(2)
    idSIButton99 = driver.find_element_by_id("idSIButton9")
    idSIButton99.submit()

    time.sleep(2)
    while True:
        try:
            if driver.find_element_by_id("idSIButton9").is_displayed():
                idSIButton99 = driver.find_element_by_id("idSIButton9")
                idSIButton99.submit()
                break
        except:
            break

    time.sleep(2)
    while True:
        try:
            if driver.find_element_by_id("passwordError").is_displayed():
                print("Signed In error!")
                raise EOFError
                break
        except EOFError:
            print('got it')
            raise
        except:
            break

    print("Signed In!")

def quit():
    print("Quitting...")
    driver.quit()

def sendMessageToSelected(groupname,content):
    print(groupname+"Grouping!")
    # 搜尋開始
    number = 0
    while True:
        number += 1
        try:
            if driver.find_element_by_xpath('//div[@data-text-as-pseudo-element="人員、群組與訊息"]').is_displayed():
                driver.find_element_by_xpath('//div[@data-text-as-pseudo-element="人員、群組與訊息"]').click()
                break
        except:
            print("沒有 人員、群組與訊息")
            pass
        print("找英文")
        try:
            if driver.find_element_by_xpath('//div[@data-text-as-pseudo-element="People, groups & messages"]').is_displayed():
                driver.find_element_by_xpath('//div[@data-text-as-pseudo-element="People, groups & messages"]').click()
                print("找到搜尋按鈕")
                break
        except:
            if number > 10:
                print("搜尋按鈕 失敗")
                break
            print("在找一次 搜尋按鈕")
            
            # 可能前面有遮蔽物  去除
            while True:
                try:
                    if driver.find_element_by_xpath('//button[@aria-label="Got it!"]').is_displayed():
                        driver.find_element_by_xpath('//button[@aria-label="Got it!"]').click()
                        break
                except:
                    print("Got it!")    
                    break
            
            time.sleep(2)
            continue


    time.sleep(1)
    # 搜尋群組
    while True:
        try:
            if driver.find_element_by_xpath('//input[@aria-label="搜尋 Skype"]').is_displayed():
                searchbar = driver.find_element_by_xpath('//input[@aria-label="搜尋 Skype"]')
                break
        except:
            pass
        
        try:
            if driver.find_element_by_xpath('//input[@aria-label="Search Skype"]').is_displayed():
                searchbar = driver.find_element_by_xpath('//input[@aria-label="Search Skype"]')
                print("開始搜尋")
                break
        except:
            break

    # 開始全字檢索
    searchbar.send_keys(groupname + Keys.RETURN)
    time.sleep(5)
    # 點 Group 的 tab
    driver.find_element_by_xpath('//button[@aria-label="Groups"]').click()
    
    try:
        if driver.find_element_by_xpath('//div[@role="presentation"]//div[contains(@aria-label,"'+groupname+'")]').is_displayed():
            driver.find_element_by_xpath('//div[@role="presentation"]//div[contains(@aria-label,"'+groupname+'")]').click()
    except:
        print("沒抓到群組="+groupname)
        str_split = groupname.split("-", 1)
        print("重新搜尋群組="+str_split[0])
        searchbar.clear()
        time.sleep(2)
        # 開始前綴字字檢索
        searchbar.send_keys(str_split[0] + Keys.RETURN)
        time.sleep(3)
        try:
            if driver.find_element_by_xpath('//div[@role="presentation"]//div[contains(@aria-label,"'+groupname+'")]').is_displayed():
                driver.find_element_by_xpath('//div[@role="presentation"]//div[contains(@aria-label,"'+groupname+'")]').click()
                print("塞選抓群組="+groupname)
                time.sleep(5)
        except:
            print("再次沒抓到群組="+groupname)
        

    # element = driver.find_element_by_xpath('//div[@aria-label="GROUP CHATS"]/div[contains(@aria-label,"'+groupname+'")]')
    # element_id = element.get_attribute("id")
    # print(element_id)
    
    # 輸入談話訊息
    print("傳送訊息")
    message = driver.find_element_by_xpath('//div[@aria-label="Type a message"]')
    arrContent = content.split('@')
    for str in arrContent:
        message.send_keys(str)
        message.send_keys(Keys.SHIFT,'\n')
    message.send_keys('\n')
    print("傳送訊息結束")




### entry ###
def main(args):

    config = configparser.ConfigParser()
    # 讀取 INI 設定檔
    # config.read('tg.ini')
    # tg_config = args.tg if args.tg else "tg_it"
    # token = config[tg_config]['token']
    # chat_id = config[tg_config]['chat_id']
    # link = "https://api.telegram.org/bot{}/sendMessage".format(token)

    # err_group = ""
    # err_message = ""

    # print(args.username)
    # print(args.password)
    # print(args.environ)

    # err_group = args.environ

    try:
        launch()
        # args = parser.parse_args()
        # print(args.username)
        time.sleep(15)

        for entry in driver.get_log('browser'):
            print(entry)

        time.sleep(15)
        time.sleep(15)

        for entry in driver.get_log('browser'):
            print(entry)
            
        time.sleep(15)
        time.sleep(15)
        time.sleep(15)
        # signIn(args.username, args.password)
        time.sleep(5)
        # print("test3: " + driver.current_url)
        # # 前面遮蔽物
        # while True:
        #     try:
        #         if driver.find_element_by_xpath('//button[@aria-label="Got it!"]').is_displayed():
        #             driver.find_element_by_xpath('//button[@aria-label="Got it!"]').click()
        #             break
        #     except:
        #         print("Got it! error")    
        #         break

        # print("1")
        # with open('./message/message', 'r') as mess:
        #     message_str = mess.read()
        #     mess.close()

        # print(message_str)
    
        # print("2")
        # # group = "【Gtigaming例行维护通知】"

        # with open('./group/'+ args.environ, 'r') as file:
        #     groupArr = file.readlines()
        #     file.close()
        # print(groupArr)

        # time.sleep(5)
        # for group in groupArr:
        #     # 搜尋開始
        #     groupname = group.replace('\n', '')
        #     print("groupname is"+ groupname)
        #     if groupname:
        #         sendMessageToSelected(groupname,message_str)
        #         time.sleep(10)

        # metadata= {
        #     "chat_id": chat_id ,
        #     "text": "skype 通知客戶" + err_group + " 通知成功 " +err_message,
        # }
        # r = requests.post( link, metadata, headers={'Content-Type':'application/x-www-form-urlencoded', 'Accept': 'application/json'})         
    # except IOError as err:
    #     print("IOError")
    #     err_message = str(err)
    #     metadata= {
    #         "chat_id": chat_id ,
    #         "text": "skype 通知客戶 通知失敗 IOError: "+ err_message,
    #     }
    #     r = requests.post( link, metadata, headers={'Content-Type':'application/x-www-form-urlencoded', 'Accept': 'application/json'}) 
    except:
        print("Something went wrong")
        
        # 暫時先註解，不讓 pipeline fail
        # os.exit(1)
    # finally:
        # quit()

if __name__ == "__main__":
    print("Starting up...")
    parser = argparse.ArgumentParser(description='pySkypeBot: Python Skype Bot')
    parser.add_argument('-u', '--username', help='Skype User Name')
    parser.add_argument('-p', '--password', help='Skype Password')
    parser.add_argument('-e', '--environ', help='Skype group')
    parser.add_argument('-t', '--tg', help='tg config name')
    main(parser.parse_args())
